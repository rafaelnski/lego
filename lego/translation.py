from modeltranslation.translator import translator, TranslationOptions
from lego.models import LegoSet


class LegoSetOptions(TranslationOptions):
    fields = ('name',)


translator.register(LegoSet, LegoSetOptions)
