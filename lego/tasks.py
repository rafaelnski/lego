import pandas as pd
import logging
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from lego.models import LegoSet, Category
from lego.utils.brick_economy import get_release_date, get_retried_set_data, get_available_set_data
from lego.utils.brick_link import get_data_from_brick_link_price_data, get_main_table_table_rows,get_row_meta_data

logger = logging.getLogger("task-info")


def get_lego_sets_from_brick_link(year):
    _, last_page = get_main_table_table_rows(year)
    res = []
    for page in range(1, last_page):
        table_rows, _ = get_main_table_table_rows(year, page=page)
        for tr in table_rows:
            row_meta = get_row_meta_data(tr)
            res.append(row_meta)

    df = pd.DataFrame(res, columns=["unique_id", "name", "categories"])
    for k, row in df.iterrows():
        lego_set, _ = LegoSet.objects.get_or_create(name=row["name"], unique_id=row["unique_id"])

        categories_table = []
        for category in row["categories"].split(","):
            category, _ = Category.objects.get_or_create(name=category)
            categories_table.append(category)

        lego_set.categories.set(categories_table)


def get_data_from_brick_link_base_data(set_unique_id):
    """ get data from brick brick link
        example url: https://www.bricklink.com/v2/catalog/catalogitem.page?S=41382-15
        updated_fields:
            - weight,
            - item_dim,
            - parts,
            - mini_figures,
            - wanted_list
    """
    weight = None
    item_dim = None
    parts = None
    mini_figures = None
    wanted_list = None

    url = "https://www.bricklink.com/v2/catalog/catalogitem.page?S={set_unique_id}#T=P".format(set_unique_id=set_unique_id)
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    html_doc = urlopen(req).read()
    soup = BeautifulSoup(html_doc, 'html.parser')

    itemId = soup.find_all("a", {"id": "_idAddToWantedLink"})[0].get("data-itemid")
    lego_set = LegoSet.objects.get(unique_id=set_unique_id)

    get_data_from_brick_link_price_data(lego_set, itemId)
    # get base info
    all_tables = soup.find_all('table')
    for table in all_tables:
        if table.find("td", {"valign": "TOP"}) is None:
            continue
        else:
            columns = table.find_all('td')
            for col in columns:
                if "Item Info" in col.text:
                    text_to_ev = col.text.replace("Item Info", "")
                    weight = int(float(text_to_ev.split("Weight:")[1].split()[0].replace("g", "")))
                    item_dim = text_to_ev.split("Item Dim.:")[1].split()[0]
                elif "Item Consists Of" in col.text:
                    text_to_ev = col.text.replace("Item Consists Of", "")
                    parts = None
                    mini_figures = None
                    if "Parts" in text_to_ev:
                        parts = int(text_to_ev.split("Parts")[0])
                    if "Minifigures" in text_to_ev:
                        mini_figures_0 = text_to_ev.split("Minifigures")[0]
                        mini_figures_splited = mini_figures_0.split()
                        mini_figures = int(mini_figures_splited[len(mini_figures_splited)-1])
                elif "My Wanted List" in col.text:
                    wanted_list = int(col.text.split("On")[1].split()[0])

    # this is table with info!
    lego_set.weight = weight
    lego_set.item_dim = item_dim
    lego_set.parts = parts
    lego_set.mini_figures = mini_figures
    lego_set.wanted_list = wanted_list
    lego_set.save()


def get_data_from_brick_economy(set_unique_id):
    """ get data from brick economy
        example url: https://www.brickeconomy.com/set/41382-15/
        updated_fields:
            - release_price,
            - release_date,
            - growth_sum,
            - predicted_growth_sum,
            - retirement_date
    """
    try:
        lego_set = LegoSet.objects.get(unique_id=set_unique_id)
    except LegoSet.DoesNotExists:
        return

    url = f"https://www.brickeconomy.com/set/{set_unique_id}/"
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    html_soup = BeautifulSoup(urlopen(req).read(), 'html.parser')

    growth_sum = None
    retirement_date = None
    predicted_growth_sum = None
    try:
        predictions_div = html_soup.find_all("div", {"id": "ContentPlaceHolder1_PanelSetPredictions"})[0]
    except IndexError:
        # must this must be retried set ! no prediction price here !
        growth_sum, release_price = get_retried_set_data(html_soup)
    else:
        release_price, retirement_date, predicted_growth_sum = get_available_set_data(html_soup, predictions_div)

    lego_set.release_price = release_price
    lego_set.release_date = get_release_date(html_soup)
    lego_set.growth_sum = growth_sum
    lego_set.predicted_growth_sum = predicted_growth_sum
    lego_set.retirement_date = retirement_date
    lego_set.save()
