import datetime
import pandas as pd
import re
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from django.db import transaction
from lego.models import LegoSet, LegoSetPrice
from lego.utils.brick_economy import MONTH_MAPPER


def clean_value(text):
    text = text.replace(u'\xa0', u'')
    text = text.replace("(Inv)", "")
    text = text.replace("\n", "")
    return text


def get_row_meta_data(row):
    row_meta = []
    for column in row:
        category_name_reg = re.compile("Sets:")
        if column.find("strong"):
            # update set name
            set_name = clean_value(column.find("strong").text)
            row_meta.append(set_name)
            # update categories
            if category_name_reg.search(column.text):
                category_names = category_name_reg.split(column.text)[1]
                if category_names.find(":"):
                    category_names = ",".join(category_names.split(":"))
                row_meta.append(clean_value(category_names))
        else:
            # update id
            other_columns = clean_value(column.text)
            if other_columns:
                row_meta.append(other_columns.strip())
    return row_meta


def get_main_table_table_rows(year, page=1):
    url = "https://www.bricklink.com/catalogList.asp?pg={page}&itemYear={year}&catType=S".format(page=page, year=year)
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    html_doc = urlopen(req).read()
    soup = BeautifulSoup(html_doc, 'html.parser')
    table = soup.find('table', attrs={'class': "catalog-list__body-main"})
    table_rows = table.find_all('tr')

    pagination = soup.find_all("div", {"class": "catalog-list__pagination--top l-clear-left"})[0]
    reg = re.compile("Page (?P<first_page>\d+) of (?P<last_page>\d+)")
    pagination_html = clean_value(pagination.text).strip()
    last_page = int(reg.search(pagination_html)[0].split("of")[1])
    return table_rows, last_page


def get_price_dataset(table_rows):
    price_dataset = []  # cols values [date_comes_from,total_qty,min_price,avg_price,max_price]
    price_header_v = None
    for idx, tr in enumerate(table_rows):
        try:
            price_header = tr.find_all('td', attrs={'class': "pcipgSubHeader"})
        except AttributeError:
            print("test")

        if price_header:
            if price_header_v and price_header_v != price_header[0].text:
                # here we can save old price set
                price_dataset.append([price_header_v, total_qty, min_price, avg_price, max_price])
            price_header_v = price_header[0].text
            continue
        elif "Total Lots:" in tr.text:
            try:
                total_qty = tr.find_all('td')[2].text
            except IndexError:
                total_qty = tr.find_all('td')[1].text
        elif "Min Price:" in tr.text:
            try:
                min_price = tr.find_all('td')[2].text.replace("PLN", "").replace(",", "")
            except IndexError:
                min_price = tr.find_all('td')[1].text.replace("PLN", "").replace(",", "")
        elif "Avg Price:" in tr.text:
            try:
                avg_price = tr.find_all('td')[2].text.replace("PLN", "").replace(",", "")
            except IndexError:
                avg_price = tr.find_all('td')[1].text.replace("PLN", "").replace(",", "")
        elif "Max Price:" in tr.text:
            try:
                max_price = tr.find_all('td')[2].text.replace("PLN", "").replace(",", "")
            except IndexError:
                max_price = tr.find_all('td')[1].text.replace("PLN", "").replace(",", "")
        else:
            continue
    return price_dataset


def get_data_from_brick_link_price_data(set_unique_id, itemId):
    price_url = "https://www.bricklink.com/v2/catalog/catalogitem_pgtab.page?idItem={itemId}&st=2&gm=1&gc=0&ei=0&prec=2&showflag=0&showbulk=0&currency=114".format(itemId=itemId)
    req = Request(price_url, headers={'User-Agent': 'Mozilla/5.0'})
    html_doc = urlopen(req).read()
    price_soup = BeautifulSoup(html_doc, 'html.parser')
    # get prices
    old_prices_for_new_sets = price_soup.find_all("td", {"class": "pcipgOddColumn"})[0]
    price_tables = old_prices_for_new_sets.find('table', attrs={'class': "pcipgInnerTable"})
    table_rows = price_tables.find_all('tr')
    dataset_1 = get_price_dataset(table_rows)
    latest_prices_for_new_sets = price_soup.find_all("td", {"class": "pcipgOddColumn"})[1]
    price_tables = latest_prices_for_new_sets.find('table', attrs={'class': "pcipgInnerTable"})
    table_rows = price_tables.find_all('tr')
    dataset_2 = get_price_dataset(table_rows)
    dataset = dataset_1 + dataset_2
    df = pd.DataFrame(dataset, columns=["date_comes_from", "total_qty", "min_price", "avg_price", "max_price"])
    lego_set = LegoSet.objects.get(unique_id=set_unique_id)
    with transaction.atomic():
        for k, row in df.iterrows():
            month, year = row["date_comes_from"].split()
            comes_from = datetime.date(day=1, month=MONTH_MAPPER.get(month), year=int(year))
            lego_price, _ = LegoSetPrice.objects.get_or_create(
                comes_from=comes_from,
                set=lego_set,
                min=float(row["min_price"]),
                max=float(row["max_price"]),
                avg=float(row["avg_price"]),
                total_sets_for_sell=int(row["total_qty"]),
            )
