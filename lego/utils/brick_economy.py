import datetime
from djmoney.money import Money


class Page404Exception(Exception):
    pass


MONTH_MAPPER = {
    "January": 1,
    "February": 2,
    "March": 3,
    "April": 4,
    "May": 5,
    "June": 6,
    "July": 7,
    "August": 8,
    "September": 9,
    "October": 10,
    "November": 11,
    "December": 12
}

DIRTY_TEXT_FIELDS = [
    "Growth",
    "%",
    "+",
    "$",
    "Retail price",
    "Set Pricing",
    "Expected Good Investment",
    "Still available at retail",
    "Retirement",
    "Annual growth",
    "Released",
]


def clean_row(row):
    """ remove unnecessary text fields """
    for field in DIRTY_TEXT_FIELDS:
        row = row.replace(field, "")
    return row


def get_retried_set_data(html_soup):
    """ in this case set is not any more available in store """
    growth_sum, release_price = None, None
    try:
        growth_div = html_soup.find_all("div", {"id": "ContentPlaceHolder1_PanelSetPricing"})[0]
    except IndexError as e:
        # must be 404 then but check here
        h3_text = html_soup.find_all("h3", {"class": "text-center"})[0]
        if h3_text.text == "The page you are looking for could not be found.":
            raise Page404Exception("404 page not found")
        else:
            raise e
    else:
        rows = growth_div.find_all("div", {"class": "row"})
        for row in rows:
            if "Growth" in row.text:
                try:
                    growth_sum = float(clean_row(row.text))
                except ValueError:
                    growth_sum = None
            if "Retail price" in row.text:
                try:
                    release_price = Money(float(clean_row(row.text)), "USD")
                except ValueError:
                    release_price = None
    return growth_sum, release_price


def get_available_set_data(html_soup, predictions_div):
    """ in this case set is still available in store """
    retirement_date = None
    predicted_growth_sum = None
    price_div = html_soup.find_all("div", {"id": "ContentPlaceHolder1_PanelSetPricing"})[0].text
    try:
        release_price = float(clean_row(price_div.split("Average price")[0]))
    except ValueError:
        release_price = None
    rows = predictions_div.find_all("div", {"class": "row"})
    for row in rows:
        if "Retirement" in row.text:
            retirement_date = clean_row(row.text)
        elif "Annual growth" in row.text:
            annual_growth = clean_row(row.text)
            try:
                first_year_split = clean_row(annual_growth.split("(first year)")[0])
                step_first = annual_growth.split("(first year)")[1]
                second_year_split = clean_row(step_first.replace("(second year)", ""))
                predicted_growth_sum = float(first_year_split) + float(second_year_split)
            except IndexError:
                first_year_split = clean_row(annual_growth.split("(Short term)")[0])
                predicted_growth_sum = float(first_year_split)
    return release_price, retirement_date, predicted_growth_sum


def get_release_date(html_soup):
    details_div = html_soup.find_all("div", {"id": "SetDetails"})[0]
    rows = details_div.find_all("div", {"class": "row"})
    release_date = None
    for row in rows:
        if "Released" in row.text:
            release_date = clean_row(row.text)
            try:
                month, year = release_date.split()
            except ValueError:
                # must be exactly date then
                month, day, year = release_date.split()
                release_date = datetime.date(day=1, month=1, year=int(year))
            else:
                release_date = datetime.date(day=1, month=MONTH_MAPPER.get(month), year=int(year))
    return release_date
