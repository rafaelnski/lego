from django.db import models
from django.utils.translation import gettext as _
from djmoney.models.fields import MoneyField
from lego.constants import InvestStage, StoreStatus


class LegoSet(models.Model):
    objects = None
    name = models.CharField(max_length=255)
    unique_id = models.CharField(unique=True, max_length=36)
    # business fields
    release_date = models.DateField(null=True)
    predicted_retirement_date = models.DateField(null=True)
    release_price = MoneyField(max_digits=14, decimal_places=2, default_currency='PLN', null=True)
    invest_stage = models.IntegerField(choices=InvestStage.INVEST_STAGE_CHOICES, null=True)
    official_store_status = models.IntegerField(choices=StoreStatus.STORE_STATUS_CHOICES, null=True)
    mini_figures = models.SmallIntegerField(null=True)
    parts = models.IntegerField(null=True)
    weight = models.IntegerField(help_text=_("Weight in g"), null=True)
    item_dim = models.CharField(null=True, max_length=36)
    wanted_list = models.IntegerField(null=True)
    growth_sum = models.FloatField(null=True)
    predicted_growth_sum = models.FloatField(null=True)
    retirement_date = models.CharField(max_length=72, null=True)
    # stats fields
    half_y_price_change = MoneyField(max_digits=14, decimal_places=2, default_currency='PLN', null=True)
    half_y_price_change_p = models.FloatField(null=True)
    latest_price = MoneyField(max_digits=14, decimal_places=2, default_currency='PLN', null=True)
    # other fields
    categories = models.ManyToManyField("lego.Category")

    def __str__(self):
        return self.name


class LegoSetPrice(models.Model):
    comes_from = models.DateField()
    set = models.ForeignKey("lego.LegoSet", on_delete=models.CASCADE)

    min = MoneyField(max_digits=14, decimal_places=2, default_currency='PLN')
    max = MoneyField(max_digits=14, decimal_places=2, default_currency='PLN')
    avg = MoneyField(max_digits=14, decimal_places=2, default_currency='PLN')

    total_sets_for_sell = models.IntegerField(null=True)

    def __str__(self):
        return f"{self.set.name} {self.avg} {self.comes_from}"


class Category(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
