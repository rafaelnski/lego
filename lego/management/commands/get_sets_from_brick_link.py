from django.core.management.base import BaseCommand
from lego.tasks import get_lego_sets_from_brick_link


class Command(BaseCommand):
    help = 'Update meta data'

    def handle(self, *args, **options):
        for year in ["2019", "2020", "2021"]:
            get_lego_sets_from_brick_link(year)
