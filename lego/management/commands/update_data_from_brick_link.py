from django.core.management.base import BaseCommand
from lego.models import LegoSet
from lego.tasks import get_data_from_brick_link_base_data


class Command(BaseCommand):
    help = 'Update meta data'

    def handle(self, *args, **options):
        for lego_set in LegoSet.objects.all():
            try:
                get_data_from_brick_link_base_data(lego_set.unique_id)
            except Exception as e:
                print(f"Error for {lego_set.unique_id}, {e}")
            else:
                print(f"set updated ! {lego_set.name}")
