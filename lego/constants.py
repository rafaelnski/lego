
class InvestStage:
    A = 1
    B = 2
    C = 3
    D = 4

    INVEST_STAGE_CHOICES = (
        (A, "A"),
        (B, "B"),
        (C, "C"),
        (D, "D")
    )


class StoreStatus:
    AVAILABLE = 1
    TEMPORARY_NOT_AVAILABLE = 2
    SOLD_OUT = 3

    STORE_STATUS_CHOICES = (
        (AVAILABLE, "Available"),
        (TEMPORARY_NOT_AVAILABLE, "Temporary not available"),
        (SOLD_OUT, "Sold out"),
    )
