from django.contrib import admin
from lego.models import LegoSet, LegoSetPrice, Category
from advanced_filters.admin import AdminAdvancedFiltersMixin
from advanced_filters import forms as adv_forms
from django.forms.formsets import DELETION_FIELD_NAME


class MyAdvancedFilterForm(adv_forms.AdvancedFilterForm):
    def clean(self):
        for form in self.fields_formset:
            if not form.is_valid():
                form.cleaned_data = {'field':'id', 'DELETE': True}
        return super().clean()

    def _should_delete_form(self, form):
        try:
            cleaned_data = form.cleaned_data.get(DELETION_FIELD_NAME, False)
        except AttributeError:
            return False
        return cleaned_data


class MyAdminAdvancedFiltersMixin(AdminAdvancedFiltersMixin):
    advanced_filter_form = MyAdvancedFilterForm


class LegoAdmin(MyAdminAdvancedFiltersMixin, admin.ModelAdmin):
    list_display = [
        "unique_id", "release_date", "predicted_retirement_date",
        "release_price", "invest_stage", "official_store_status", "mini_figures",
        "parts", "weight", "item_dim", "wanted_list", "growth_sum", "predicted_growth_sum",
        "retirement_date", "half_y_price_change", "half_y_price_change_p", "latest_price"
    ]
    advanced_filter_fields = list_display


admin.site.register(LegoSet, LegoAdmin)
admin.site.register(LegoSetPrice, admin.ModelAdmin)
admin.site.register(Category, admin.ModelAdmin)
