import pandas as pd
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup


class NoTableFound(ValueError):
    pass


class TableException(Exception):
    pass


class Scrapper:

    def clean_field(self, value):
        if not isinstance(value, str):
            return

        value = value.strip().replace(" ", "")
        try:
            value = float(value)
        except ValueError:
            value = value.replace(u'\xa0', u' ')
            value = value.strip().replace(" ", "")

            # case xxx,xxxx
            try:
                value, _ = value.split(",")
            except ValueError:
                pass

            try:
                value = float(value)
            except ValueError:
                pass

        return value

    def _map_html_data_to_model(self, df, mapper):
        cleaned_data = []
        for k, row in df.iterrows():
            cleaned_d = {}
            for column in df.columns:
                try:
                    db_field = mapper[column]
                except KeyError:
                    continue
                clean_method = getattr(self, f"clean_{db_field}", None)
                value = self.clean_field(row[column])
                value = clean_method(value, row=row) if clean_method else value # noqa
                cleaned_d[db_field] = value

            cleaned_data.append(cleaned_d)

        cleaned_data = pd.DataFrame(cleaned_data, columns=mapper.values())
        return cleaned_data

    def pandas_get_table(self, url, mapper, table=None):
        # df_list[0,1,2...] tables

        try:
            df_list = pd.read_html(url)
        except ValueError:
            raise NoTableFound

        if len(df_list) > 1 and table is None:
            raise TableException("Pick table")
        else:
            table = 0

        df = self._map_html_data_to_model(df_list[table], mapper)
        return df

    def bt4_get_table(self, url, mapper, table_class):
        req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
        html_doc = urlopen(req).read()
        # html_doc = requests.get(url).content
        soup = BeautifulSoup(html_doc, 'html.parser')
        table = soup.find('table', attrs={'class': table_class})
        table_rows = table.find_all('tr')

        res = []
        for tr in table_rows:
            td = tr.find_all('td')
            row = [tr.text.strip() for tr in td if tr.text.strip()]
            if row:
                res.append(row)

        df = pd.DataFrame(res, columns=mapper.keys())
        df = self._map_html_data_to_model(df, mapper)
        return df
