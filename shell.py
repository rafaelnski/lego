import django
django.setup()

from lego.tasks import get_data_from_brick_economy, get_data_from_brick_link_base_data
from lego.models import LegoSet
from lego.utils.brick_economy import Page404Exception


def update_data_from_brick_link():
    for lego_set in LegoSet.objects.all():
        try:
            get_data_from_brick_link_base_data(lego_set.unique_id)
        except Exception as e:
            print(f"Error for {lego_set.unique_id}, {e}")
        else:
            print(f"set updated ! {lego_set.name}")


def update_data_from_brick_economy():
    for lego_set in LegoSet.objects.all():
        try:
            get_data_from_brick_economy(lego_set.unique_id)
        except Page404Exception:
            print(f"404 page not found for {lego_set.unique_id}")
        except Exception as e:
            print(f"Error for {lego_set.unique_id}, {e}")
        else:
            print(f"set updated ! {lego_set.unique_id}")


def get_set_info_force():
    """11922-1"""
    # 11922-1
    # 11926-1 ----> 10928-1.
    get_data_from_brick_economy("10928-1")


# get_set_info_force()
# update_data_from_brick_economy()

update_data_from_brick_link()
